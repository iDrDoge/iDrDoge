<h1>iDrDoge</h1>

## Languages
[![Javascript](https://img.shields.io/badge/Javascript-1e1e1e.svg?&style=for-the-badge&logo=javascript&logoColor=23f7df1e)](https://nodejs.org/en/)
[![Typescript](https://img.shields.io/badge/Typescript-1e1e1e.svg?&style=for-the-badge&logo=typescript&logoColor=007acc)](https://www.typescriptlang.org/)

## Projects
[![Frostnite](https://img.shields.io/badge/Frostnite-1e1e1e.svg?&style=for-the-badge&logo=epicgames&logoColor=white)](https://frostnite.dev)
